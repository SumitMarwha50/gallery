package com.example.gallery.feature.gallery.module

import com.example.gallery.feature.gallery.fragment.GalleryFolderFragment
import com.example.gallery.feature.gallery.fragment.GalleryImageFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class MainActivityModule {

    @ContributesAndroidInjector
    abstract fun contributeGalleryFolderFragment(): GalleryFolderFragment

    @ContributesAndroidInjector
    abstract fun contributeGalleryImageFragment(): GalleryImageFragment
}