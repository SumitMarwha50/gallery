package com.example.gallery.feature.gallery.dto

data class ImageFolder(
    var path: String? = null,
    var folderName: String? = null,
    var numberOfPics: Int = 0,
    var firstPic: String? = null
) {
    fun addpics() {
        numberOfPics += 1
    }
}