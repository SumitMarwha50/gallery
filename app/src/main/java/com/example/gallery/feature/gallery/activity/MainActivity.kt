package com.example.gallery.feature.gallery.activity

import android.os.Bundle
import android.util.Log
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import com.example.gallery.core.base.BaseViewModel
import com.example.gallery.core.base.DaggerBaseActivity
import com.example.gallery.R
import com.example.gallery.databinding.ActivityMainBinding

class MainActivity : DaggerBaseActivity() {

    private lateinit var activityBinding: ActivityMainBinding

    private val baseViewModel: BaseViewModel by lazy {
        ViewModelProvider(this, viewModelFactory).get(BaseViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        navController = Navigation.findNavController(this@MainActivity, R.id.nav_host_fragment)
        setObserver()
    }

    private fun setObserver() {
        baseViewModel.navigationWithBundle.observe(this, Observer {
            it?.let {
                try {
                    navController?.navigate(it.first, it.second)
                } catch (e: IllegalArgumentException) {
                    Log.e("Exception", e.toString())
                }
            }
        })
    }
}
