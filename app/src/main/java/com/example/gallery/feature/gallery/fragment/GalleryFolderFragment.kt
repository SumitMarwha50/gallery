package com.example.gallery.feature.gallery.fragment

import android.Manifest
import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.gallery.R
import com.example.gallery.core.base.BaseViewModel
import com.example.gallery.core.base.DaggerBaseFragment
import com.example.gallery.core.utils.snackBarWithAction
import com.example.gallery.databinding.FragmentGalleryFolderBinding
import com.example.gallery.feature.gallery.viewmodel.GalleryViewModel
import com.google.android.material.snackbar.Snackbar
import permissions.dispatcher.NeedsPermission
import permissions.dispatcher.OnPermissionDenied
import permissions.dispatcher.RuntimePermissions

@RuntimePermissions
class GalleryFolderFragment : DaggerBaseFragment<GalleryViewModel, FragmentGalleryFolderBinding>() {

    override fun layoutId(): Int = R.layout.fragment_gallery_folder

    override fun bindVariables() {
        viewBinding.viewModel = viewModel
    }

    override fun initializeViewModel(): GalleryViewModel {
        return ViewModelProvider(
            requireActivity(),
            viewModelFactory
        ).get(GalleryViewModel::class.java)
    }

    private val baseViewModel: BaseViewModel by lazy {
        ViewModelProvider(requireActivity(), viewModelFactory).get(BaseViewModel::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        readStorageWithPermissionCheck()
        setObserver()
    }

    @NeedsPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
    fun readStorage() {
        viewModel.setGalleryFolder()
    }

    @OnPermissionDenied(Manifest.permission.READ_EXTERNAL_STORAGE)
    fun onStorageDenied() {
        R.string.need_storage_permission.snackBarWithAction(viewBinding.root,
            R.string.retry, View.OnClickListener {
                readStorageWithPermissionCheck()
            }, Snackbar.LENGTH_INDEFINITE)
    }

    private fun setObserver() {
        viewModel.folderSelected.observe(viewLifecycleOwner, Observer {
            val bundle = bundleOf(getString(R.string.folderPath) to it)
            baseViewModel.setNavigationWithBundle(R.id.galleryFragment, bundle)
        })
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        // NOTE: delegate the permission handling to generated function
        onRequestPermissionsResult(requestCode, grantResults)
    }
}