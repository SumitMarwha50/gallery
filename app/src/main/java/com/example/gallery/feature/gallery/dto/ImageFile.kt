package com.example.gallery.feature.gallery.dto

data class ImageFile(
    var pictureName: String? = null,
    var picturePath: String? = null,
    var pictureSize: String? = null,
    var pictureHeight: Int? = 0,
    var imageUri: String? = null,
    var selected: Boolean = false
)