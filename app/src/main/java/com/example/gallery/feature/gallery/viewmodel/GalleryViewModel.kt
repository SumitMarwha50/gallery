package com.example.gallery.feature.gallery.viewmodel

import android.content.ContentResolver
import android.provider.MediaStore
import android.util.Log
import androidx.databinding.ObservableArrayList
import com.example.gallery.core.base.BaseViewModel
import com.example.gallery.BR
import com.example.gallery.R
import com.example.gallery.core.utils.SingleLiveEvent
import com.example.gallery.feature.gallery.dto.ImageFile
import com.example.gallery.feature.gallery.dto.ImageFolder
import me.tatarka.bindingcollectionadapter2.ItemBinding
import java.util.*
import javax.inject.Inject

class GalleryViewModel @Inject constructor(
    private val contentResolver: ContentResolver
): BaseViewModel() {

    var callback = object : Callback {
        override fun onClick(path: String) {
            folderSelected.postValue(path)
        }
    }

    val folderSelected = SingleLiveEvent<String>()
    val galleryFolder = ObservableArrayList<ImageFolder>()
    var galleryItemBinding = ItemBinding.of<ImageFolder>(BR.folder, R.layout.images_folder)
        .bindExtra(BR.callback, callback)
    val galleryImages = ObservableArrayList<ImageFile>()
    var galleryImageItemBinding = ItemBinding.of<ImageFile>(BR.image, R.layout.images_file)

    fun setGalleryFolder() {
        getPicturePaths()?.let {
            galleryFolder.clear()
            galleryFolder.addAll(it)
        }
    }

    fun setImages(path: String) {
        getAllImagesByFolder(path)?.let {
            galleryImages.clear()
            galleryImages.addAll(it)
        }
    }

    private fun getPicturePaths(): ArrayList<ImageFolder>? {
        val picFolders: ArrayList<ImageFolder> = ArrayList()
        val picPaths = ArrayList<String>()
        val allImagesUri =
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        val projection = arrayOf(
            MediaStore.Images.ImageColumns.DATA, MediaStore.Images.Media.DISPLAY_NAME,
            MediaStore.Images.Media.BUCKET_DISPLAY_NAME, MediaStore.Images.Media.BUCKET_ID
        )
        val cursor = contentResolver.query(allImagesUri, projection, null, null, null)
        try {
            cursor?.moveToFirst()
            do {
                val folds = ImageFolder()
                val name = cursor!!.getString(
                    cursor.getColumnIndexOrThrow(
                        MediaStore.Images.Media.DISPLAY_NAME
                    )
                )
                val folder = cursor.getString(
                    cursor.getColumnIndexOrThrow(
                        MediaStore.Images.Media.BUCKET_DISPLAY_NAME
                    )
                )
                val dataPath = cursor.getString(
                    cursor.getColumnIndexOrThrow(
                        MediaStore.Images.Media.DATA
                    )
                )
                var folderPaths = dataPath.substring(0, dataPath.lastIndexOf("$folder/"))
                folderPaths = "$folderPaths$folder/"
                if (!picPaths.contains(folderPaths)) {
                    picPaths.add(folderPaths)
                    folds.path = folderPaths
                    folds.folderName = folder
                    folds.firstPic = dataPath
                    folds.addpics()
                    picFolders.add(folds)
                } else {
                    for (i in picFolders.indices) {
                        if (picFolders[i].path.equals(folderPaths)) {
                            picFolders[i].firstPic = dataPath
                            picFolders[i].addpics()
                        }
                    }
                }
            } while (cursor?.moveToNext() == true)
            cursor.close()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        for (i in picFolders.indices) {
            Log.d(
                "picture folders",
                picFolders[i].folderName.toString() + " and path = " + picFolders[i].path + " " + picFolders[i].numberOfPics
            )
        }
        return picFolders
    }

    private fun getAllImagesByFolder(path: String): ArrayList<ImageFile?>? {
        var images: ArrayList<ImageFile?> = ArrayList()
        val allVideosUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        val projection = arrayOf(
            MediaStore.Images.ImageColumns.DATA, MediaStore.Images.Media.DISPLAY_NAME,
            MediaStore.Images.Media.SIZE
        )
        val cursor = contentResolver.query(
            allVideosUri,
            projection,
            MediaStore.Images.Media.DATA + " like ? ",
            arrayOf("%$path%"),
            null
        )
        try {
            cursor?.moveToFirst()
            do {
                val pic = ImageFile()
                pic.pictureName = cursor?.getString(cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DISPLAY_NAME))
                pic.picturePath = cursor?.getString(cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA))
                pic.pictureSize = cursor?.getString(cursor.getColumnIndexOrThrow(MediaStore.Images.Media.SIZE))
                images.add(pic)
            } while (cursor?.moveToNext() == true)
            cursor?.close()
            val reSelection: ArrayList<ImageFile?> = ArrayList()
            for (i in images.size - 1 downTo -1 + 1) {
                reSelection.add(images[i])
            }
            images = reSelection
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
        return images
    }

    interface Callback {
        fun onClick(path: String)
    }
}