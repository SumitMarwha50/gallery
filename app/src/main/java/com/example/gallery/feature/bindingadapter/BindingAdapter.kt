package com.example.gallery.feature.bindingadapter

import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.gallery.R

@BindingAdapter(value = ["app:cacheSize"])
fun staggeredAdapter(recyclerView: RecyclerView, cacheSize: Int) {
    val layoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
    layoutManager.gapStrategy = StaggeredGridLayoutManager.GAP_HANDLING_NONE
    recyclerView.layoutManager = layoutManager
    recyclerView.itemAnimator = null
    recyclerView.setHasFixedSize(true)
    recyclerView.setItemViewCacheSize(cacheSize)
    recyclerView.setDrawingCacheEnabled(true)
    recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_AUTO)
}

@BindingAdapter("app:imageUrl")
fun appImageUrl(imageView: ImageView, imageUrl: String?) {
    imageUrl?.let {
        Glide.with(imageView.context).load(imageUrl)
            .placeholder(R.color.white)
            .dontTransform().dontAnimate().fitCenter()
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .into(imageView)
    }
}