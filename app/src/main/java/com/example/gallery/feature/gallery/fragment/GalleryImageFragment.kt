package com.example.gallery.feature.gallery.fragment

import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProvider
import com.example.gallery.core.base.DaggerBaseFragment
import com.example.gallery.R
import com.example.gallery.databinding.FragmentGalleryImagesBinding
import com.example.gallery.feature.gallery.viewmodel.GalleryViewModel

class GalleryImageFragment: DaggerBaseFragment<GalleryViewModel, FragmentGalleryImagesBinding>() {

    override fun layoutId(): Int = R.layout.fragment_gallery_images

    override fun bindVariables() {
        viewBinding.viewModel = viewModel
    }

    override fun initializeViewModel(): GalleryViewModel {
        return ViewModelProvider(requireActivity(), viewModelFactory).get(GalleryViewModel::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.getString(getString(R.string.folderPath))?.let {
            viewModel.setImages(it)
        }
    }

}