package com.example.gallery

import com.example.gallery.core.di.AppComponent
import com.example.gallery.core.di.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication

class GalleryApplication: DaggerApplication() {

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        appComponent = DaggerAppComponent.builder().application(this).build()
        return appComponent
    }

    companion object {
        private lateinit var sInstance: GalleryApplication
        private lateinit var appComponent: AppComponent

        @Synchronized
        fun getInstance(): GalleryApplication {
            return sInstance
        }

        @Synchronized
        fun getCoreComponent(): AppComponent {
            return appComponent
        }
    }
}