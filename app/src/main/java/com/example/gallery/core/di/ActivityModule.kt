package com.example.gallery.core.di

import com.example.gallery.core.base.DaggerBaseActivity
import com.example.gallery.feature.gallery.activity.MainActivity
import com.example.gallery.feature.gallery.module.MainActivityModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class ActivityModule {

    @ContributesAndroidInjector
    abstract fun contributeDaggerBaseActivity(): DaggerBaseActivity

    @ContributesAndroidInjector(modules = [MainActivityModule::class])
    abstract fun contributeMainActivity(): MainActivity
}