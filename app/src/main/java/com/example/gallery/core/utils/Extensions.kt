package com.example.gallery.core.utils

import android.view.View

/**
 * Extension method to provide simpler access to {@link View#getResources()#getString(int)}.
 */
fun View.toString(stringResId: Int): String = resources.getString(stringResId)

/**
 * Extension method to provide simpler access to {@link View#getResources()#getString(int)}.
 */
fun View.getResourceName(stringResId: Int): String? = resources.getResourceName(stringResId)
