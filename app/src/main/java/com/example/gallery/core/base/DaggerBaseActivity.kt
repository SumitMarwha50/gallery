package com.example.gallery.core.base

import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

abstract class DaggerBaseActivity: DaggerAppCompatActivity() {

    var navController: NavController? = null

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

}