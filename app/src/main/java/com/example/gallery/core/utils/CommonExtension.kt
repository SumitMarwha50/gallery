package com.example.gallery.core.utils

import android.content.res.Resources
import android.view.View
import androidx.core.content.ContextCompat
import com.example.gallery.R
import com.google.android.material.snackbar.Snackbar

fun Any?.snackBarWithAction(
    view: View,
    actionMessageId: Int,
    onClickListener: View.OnClickListener?,
    duration: Int = Snackbar.LENGTH_LONG
): Snackbar {
    var errorMsg = view.toString(R.string.something_went_wrong_please_try)
    this?.let {
        when (it) {
            is String -> errorMsg = it
            is Int -> {
                try {
                    view.getResourceName(it)?.let { stringValue ->
                        errorMsg = view.toString(it)
                    }
                } catch (e: Resources.NotFoundException) {
                }
            }
            else -> {
                errorMsg = view.toString(R.string.something_went_wrong_please_try)
            }
        }
    }

    val snackBar = Snackbar.make(view, errorMsg, duration)
    snackBar.setAction(actionMessageId, onClickListener)
        .setActionTextColor(ContextCompat.getColor(view.context, R.color.colorAccent))
    return snackBar.apply { show() }
}