package com.example.gallery.core.di

import android.app.Application
import android.content.ContentResolver
import android.content.Context
import com.example.gallery.core.di.ViewModelModule
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = [ViewModelModule::class])
class AppModule {

    @Provides
    @Singleton
    fun provideApplicationContext(app: Application): Context {
        return app.applicationContext
    }

    @Provides
    @Singleton
    fun provideContextResolver(context: Context): ContentResolver {
        return context.contentResolver
    }
}